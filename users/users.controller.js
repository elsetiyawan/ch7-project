const usersModel = require("./users.model");
const jwtTool = require("jsonwebtoken");

class userController {
  // controller record new data to userGamesTable
  postUserGames = async (req, res) => {
    // try to connect to db
    try {
      // request data body
      const dataReq = req.body;
      //   kirim request ke userModel
      const postUserDataReq = await usersModel.pushUserGames(dataReq);
      console.log(dataReq);

      return res
        .status(200)
        .json({ message: `success to insert new record to table userGames` });
    } catch (error) {
      console.log(error);
      // if error occurs
      res.status(500).json({ message: "internal server error" });
    }
  };
  // // controller get all data
  getAllData = async (req, res) => {
    // try connect to db
    try {
      // get all data menggunakan usersModel getAllUsers
      const getAllData = await usersModel.getAllUsers();
      console.log(getAllData);
      return res.status(200).json(getAllData);
    } catch (error) {
      // if error occurs
      res.status(500).json({ message: "internal server error" });
    }
  };

  //controller to login with username and password
  loginUsers = async (req, res) => {
    try {
      const { username, password } = req.body;
      const findData = await usersModel.verifyUserGames(username, password);
      if (findData) {
        try {
          // verify my JWT token to login
          const createToken = jwtTool.sign(
            findData.dataValues,
            "jwt-token-saya",
            { expiresIn: "1d" }
          );
          console.log(createToken);

          return res.json({ accessToken: createToken });
        } catch (error) {
          console.log(error);
          return res.status(500).json({ message: "token not created" });
        }
      } else {
        return res.status(404).json({ message: "credentials not found" });
      }
    } catch (error) {
      console.log(error);
      res.status(404).json({ message: "internal server error" });
    }
  };

  //   controller update user biodata
  updateUserBio = async (req, res) => {
    try {
      // get user_id menggunakan request params
      const { user_id } = req.params;

      //   get data using request body
      const { firstname, lastname, fullname, address } = req.body;

      // check if user_id is already exists
      const exist = await usersModel.dataUserBioExists(user_id);

      if (exist) {
        await usersModel.updateUserBio(
          firstname,
          lastname,
          fullname,
          address,
          user_id
        );

        return res.status(200).json({
          message: `User biodata ${firstname} and id ${user_id}  updated`,
        });
      } else {
        const insertUser = await usersModel.insertUserBio(
          firstname,
          lastname,
          fullname,
          address,
          user_id
        );
        console.log(user_id);
        return res.status(200).json(insertUser);
      }
    } catch (error) {
      console.log(error);
      return res.status(500).json({ message: "internal server error" });
    }
  };
  // controller to get single user
  getSingleUserBio = async (req, res) => {
    try {
      const { user_id } = req.params;
      const userExist = await usersModel.getSingleUserBio(user_id);
      if (userExist !== null) {
        console.log(user_id);
        res.statusCode = 200;
        return res.json(userExist);
      } else {
        return res.json({ message: "user_id not exists" });
      }
    } catch (error) {
      res.statusCode = 500;
      return res.json({ message: `internal server error` });
    }
  };
}
module.exports = new userController();
