const express = require("express");
const userController = require("./users.controller");
const authMiddleWare = require("../middleware/authMiddleware");
// const { checkSchema } = require("express-validator");

// const { query, validationResult } = require("express-validator");
// const schemaValidation = require("../middleware/SchemaValidation");
// const {
//   registrationSchema,
//   loginSchema,
//   updateSchema,
//   getByUserIdSchema,
// } = require("./users.schema");

// create a router for users
const userRouter = express.Router();

//routing get all data in database
userRouter.get("/", userController.getAllData);
// // routing post data to userGames table
// userRouter.post(
//   "/signup",
//   checkSchema(registrationSchema),
//   schemaValidation,
//   userController.postUserGames
// );

// // routing login to userGames
// userRouter.post(
//   "/login",
//   checkSchema(loginSchema),
//   schemaValidation,
//   userController.loginUsers
// );

// // update user_biodate router
// userRouter.put(
//   "/detail/:user_id",
//   authMiddleWare,
//   checkSchema(updateSchema),
//   schemaValidation,

//   userController.updateUserBio
// );

// // get data user biodata by user_id
// userRouter.get(
//   "/detail/:user_id",
//   authMiddleWare,
//   checkSchema(getByUserIdSchema),
//   schemaValidation,
//   userController.getSingleUserBio
// );

module.exports = userRouter;
