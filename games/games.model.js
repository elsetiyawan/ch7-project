const db = require("../db/models");

class gameModel {
  //post new record to gameRooms table
  newRecordGameRooms = async (player1Choice, roomName, idPlayer1) => {
    const recordRoom = await db.gameRooms.create({
      player1Choice,
      roomName,
      idPlayer1,
    });

    return recordRoom;
  };
  // get rooms by id model
  getRoomById = async (id) => {
    const getRoom = await db.gameRooms.findOne({
      where: { id: id },
      include: [{ model: db.UserGames, as: "player2" }, { model: db.UserGames, as : "player1" }]
    });
    return getRoom;
  };

  // get all rooms
  getAllRooms = async () => {
    const getAll = await db.gameRooms.findAll({
      include:  [{ model: db.UserGames, as: "player2" }, { model: db.UserGames, as : "player1" }]
    });
    return getAll;
  };

  // update roomGame when player to joined model
  updateRoom = async (idPlayer2, result, player2Choice, id) => {
    const updateRoom = await db.gameRooms.update(
      { idPlayer2, result, player2Choice },
      { where: { id: id } }
    );
    return updateRoom;
  };

  // get all data game histories
  getAllGameHistory = async () => {
    return await db.game_history.findAll();
  };

  // create new record game histories
  recordNewGameHistory = async (opponent, user_id, result) => {
    const newRecord = await db.game_history.create({
      opponent,
      user_id,
      result,
    });
    return newRecord;
  };
}
module.exports = new gameModel();
