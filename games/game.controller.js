const gameModel = require("./games.model");
const usersModel = require("../users/users.model");
const jwtTool = require("jsonwebtoken");
const { validationResult } = require("express-validator");

class gameController {
  // create new record for new rooms and player 1 choice controller
  recordNewRoom = async (req, res) => {
    try {
      const { roomName, player1Choice, idPlayer1 } = req.body;
      const pushDataReq = await gameModel.newRecordGameRooms(
        player1Choice,
        roomName,
        idPlayer1
      );

      res.json({
        message: `player with id  ${idPlayer1} has choice ${player1Choice} and created ${roomName}`,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({ message: "internal server error" });
    }
  };

  // get rooms by id controller
  getRoomsById = async (req, res) => {
    try {
      const { id } = req.params;
      const getRoom = await gameModel.getRoomById(id);

      res.status(200).json(getRoom);
    } catch (error) {
      console.log(error);
      return res.status(500).json({ message: "internal server error" });
    }
  };

  // get all rooms controllers
  getAllRooms = async (req, res) => {
    try {
      const getAllRooms = await gameModel.getAllRooms();
      return res.json(getAllRooms);
    } catch (error) {
      return res.status(500).json({ message: "internal server error" });
    }
  };

  // update gameRoom when player 2 joining game controller
  updateGameHistories = async (req, res) => {
    try {
      const { id } = req.params;

      const { idPlayer2, result, player2Choice, opponent, user_id } = req.body;
      const updateGameRoom = await gameModel.updateRoom(
        idPlayer2,
        result,
        player2Choice,
        id
      );
      const insertGameHistories = await gameModel.recordNewGameHistory(
        opponent,
        user_id,
        result
      );
      res.status(200).json({
        message: `${idPlayer2}, ${result}, ${player2Choice} joining in room with room id ${id}`,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: "internal server error" });
    }
  };

  // get all game_histories
  getAllGameHistories = async (req, res) => {
    try {
      const allData = await gameModel.getAllGameHistory();

      res.status(200).json({ data: allData });
    } catch (error) {
      res.status(500).json({ message: "internal server error" });
    }
  };

  // new record to games_histories table controller
  postGameHistories = async (req, res) => {
    // try to create new record
    try {
      const { opponent, user_id, result } = req.body;
      const newRecord = await gameModel.recordNewGameHistory(
        opponent,
        user_id,
        result
      );
      res.status(200).json({
        data: newRecord,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: `internal server error` });
    }
  };
}
module.exports = new gameController();
