const express= require('express');
const userRouter = require('./users/users.route')
// const gameRouter = require('./games/games.route');
// // const swaggerUi = require('swagger-ui-express');

// const swaggerDocument = require('./GameRPS.json');

const app = express();
let cors = require('cors')

const PORT = 3000
app.use(cors())
// app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(express.json());


// testing
app.get("/", (req, res) =>{
    res.send("Hello World")
})
// routing to /users
app.use("/users", userRouter);
// // routing to /game
// app.use("/games",gameRouter)

app.listen(PORT,() => {
    console.log(`listening on port ${PORT}`);
});
module.exports = app




