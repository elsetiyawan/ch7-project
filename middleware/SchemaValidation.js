const { validationResult } = require("express-validator");

const schemaValidation = async (req, res,next) => {
     // check validation result dari data request
     const result = validationResult(req)
     if (result.isEmpty()) {
       next()
     }else{
       console.log(result)
       res.status(400).json({errors : result.errors})
     }

};
module.exports = schemaValidation
