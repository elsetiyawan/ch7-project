const jwtTool = require("jsonwebtoken");

const authMiddleware = async (req, res, next) => {
  try {
    // check if authorization is undifined
    const { authorization } = req.headers;
    if (authorization === undefined) {
      return res.status(403).json({ message: "Unauthorized" });
    }
    const splittedToken = authorization.split(" ")[1]
    console.log(splittedToken)
    //   check if authorization is valid
    const tokenVerified = await jwtTool.verify(splittedToken, "jwt-token-saya");
    req.user = tokenVerified;
    next();
  } catch (error) {
    return res.status(403).json({ message: "invalid token" });
  }
};
module.exports = authMiddleware;
